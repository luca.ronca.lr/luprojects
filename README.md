# LuProjects
Raccolta dei progetti di Luca Ronca

## ARtista
Progetto realizzato in contesto extrascolastico con l'aiuto di:
 - Davide Sabetta
 - Giovanni Tedesco
 - Iris Campana
 - William Draicchio
 - Andrea Marchese

Applicazione per VR che consente di pitturare su una tela virtuale.

Il progetto si è candidato tra i finalisti del concorso FABER 2023.

Io mi occupo della parte di programmazione delle funzionalità e della gestione delle scene di Unity. **E' ancora in via di sviluppo**.

## HoopsVR 
Lavoro di gruppo realizzato in contesto scolastico. Partecipanti:
 - Davide Sabetta
 - Enrico Giangioppo
 - Gabriele Lisa
 - Alessandro Burriesci
 - Paolo Forgione
 
Semplice gioco per il VR in cui lo scopo è riuscire a fare 4 canestri consencutivi da diverse posizioni.

Io mi sono occupato della parte di gestione dei round, della gestione dei punteggi e delle animazioni dei tifosi nella scena Unity.

## MadShip
Esame di metà corso del modulo di Unity3D. Semplice gioco in cui bisogna schivare con una navicella spaziale degli asteroidi generati casualmente.
Mi sono occupato della programmazione, gli asset in scena sono stati forniti dal professore.

## LuTanks
Primo esercizio svolto con Unity. Semplice gioco per 2 giocatori dove l'obiettivo è distruggere il carro armato avversario.
Mi sono occupato sia della modellazione e che della programmazione.