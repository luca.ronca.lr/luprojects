using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletHit : MonoBehaviour
{
    [SerializeField] private GameObject target, bullet;
    private int playerLife;
    private void OnCollisionEnter(Collision collision)
    {
        if(target.name == "PlayerTwo")
        {
            if(collision.gameObject.Equals(target))
            {
                playerLife = PlayerPrefs.GetInt("PlayerTwoLife") - 1;
                PlayerPrefs.SetInt("PlayerTwoLife", playerLife);
                Debug.Log("Hit confirmed");
                Debug.Log(playerLife.ToString());
                if(playerLife == 0)
                {   
                    Destroy(collision.gameObject);
                    Destroy(target);
                    PlayerPrefs.SetString("Winner", PlayerPrefs.GetString("PlayerOneName"));
                }
            }
        }
        else
        {
            if (collision.gameObject.Equals(target))
            {
                playerLife = PlayerPrefs.GetInt("PlayerOneLife") - 1;
                PlayerPrefs.SetInt("PlayerOneLife", playerLife);
                Debug.Log("Hit confirmed");
                Debug.Log(playerLife.ToString());
                if (playerLife == 0)
                {
                    Destroy(collision.gameObject);
                    Destroy(target);
                    PlayerPrefs.SetString("Winner",PlayerPrefs.GetString("PlayerTwoName"));
                }
            }
        }
        

        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Player")) 
            Destroy(bullet);
    }
}
