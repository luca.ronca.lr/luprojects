using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] private GameObject reloadBuff1, reloadBuff2;
    [SerializeField] private float startTime;
    private float currentTime;
    void Start()
    {
        currentTime = startTime;
    }
    private void Update()
    {
        Timer();
    }
    void Timer()
    {
        currentTime -= Time.deltaTime;

        if (currentTime < 0)
        {
            currentTime = 0;
            Debug.Log("End game");
            reloadBuff1.SetActive(true);
            reloadBuff2.SetActive(true);
        }
    }
}
