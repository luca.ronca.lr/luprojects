using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GUIMenuManager : MonoBehaviour
{
    private string _pOneName = "P1", _pTwoName = "P2";
    //private Tank _tankOne, _tankTwo;

    //[SerializeField] GameObject lTank, mTank, hTank;
    private void Start()
    {
        //Default name
        PlayerPrefs.SetString("PlayerOneName", "Player1");
        PlayerPrefs.SetString("PlayerTwoName", "Player2");
        //Tanks
        //Tank lightTank = new Tank(8, 200, 1.2f);
        //lightTank.SetType(lTank);
        //Tank midTank = new Tank(5, 150, 1f);
        //midTank.SetType(mTank);
        //Tank heavyTank = new Tank(3, 125, 0.8f);
        //heavyTank.SetType(hTank);
    }
    public void PlayButton()
    {
        Debug.Log("Play");
        //Player
        //var playerOne = new Player(_pOneName, _tankOne);
        //var playerTwo = new Player(_pTwoName, _tankTwo);
        PlayerPrefs.SetString("PlayerOneName", _pOneName);
        PlayerPrefs.SetString("PlayerTwoName", _pTwoName);

        SceneManager.LoadScene("SimpleBattleGround");
    }
    public void OptionsButton() 
    {
        Debug.Log("Options");
    }
    public void ExitButton() 
    {
        Debug.Log("Quit");
        Application.Quit();
    }
    public void InsertPlayerOneName(string name)
    {
        Debug.Log("Player1: " + name);
        _pOneName = name;
    }
    public void InsertPlayerTwoName(string name)
    {
        Debug.Log("Player2: " + name);
        _pTwoName = name;
    }
}
