using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTankCommands : MonoBehaviour
{
    //Stats
    [SerializeField] private int player;
    public int playerLife;
    public float speed;
    public float rotation;
    public float reloadTime; 
    private float currentTime, buffTime = 10;
    private bool load, powerUp;

    //Bullet
    [SerializeField] private Transform bulletSpawnPoint;
    [SerializeField] private GameObject bulletPrefab;
    public float bulletSpeed;
    private void Start()
    {
        if(player == 1)
            PlayerPrefs.SetInt("PlayerOneLife", playerLife);
        else
            PlayerPrefs.SetInt("PlayerTwoLife", playerLife);
        load = true;
        powerUp = false;
    }
    void Update()
    {
        switch(player)
        {
            case 1:
                MovePlayerOne();
                ShootingPlayerOne();
                break;
            case 2:
                MovePlayerTwo();
                ShootingPlayerTwo();
                break;
            default:
                Debug.Log("Errore");
                break;
        }
        if(powerUp.Equals(true))
        {
            Buff();
        }
    }
    void MovePlayerOne()
    {
        if (Input.GetKey(KeyCode.W))
            transform.Translate(speed * Time.deltaTime * Vector3.forward);
        if (Input.GetKey(KeyCode.S))
            transform.Translate(speed * Time.deltaTime * Vector3.back);
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.down, rotation * Time.deltaTime);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.up, rotation * Time.deltaTime);
    }
    void MovePlayerTwo()
    {
        if (Input.GetKey(KeyCode.UpArrow))
            transform.Translate(speed * Time.deltaTime * Vector3.forward);
        if (Input.GetKey(KeyCode.DownArrow))
            transform.Translate(speed * Time.deltaTime * Vector3.back);
        if (Input.GetKey(KeyCode.LeftArrow))
            transform.Rotate(Vector3.down, rotation * Time.deltaTime);
        if (Input.GetKey(KeyCode.RightArrow))
            transform.Rotate(Vector3.up, rotation * Time.deltaTime);    
    }
    void Cannon()
    {
        var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
        bullet.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.forward * bulletSpeed;
        load = false;
    }
    void Reload()
    {
        currentTime -= Time.deltaTime;
        if (currentTime < 0)
        {
            load = true;
            currentTime = reloadTime;
        }
    }
    void ShootingPlayerOne()
    {
        if(load == true)
        {
            if(Input.GetKeyDown(KeyCode.Q))
            { 
                Debug.Log("PlayerOne: BANG!");
                Cannon();
            }
        }
        else 
            Reload();
    }
    void ShootingPlayerTwo()
    {
        if(load == true)
        {
            if (Input.GetKeyDown(KeyCode.RightShift))
            {
                Debug.Log("PlayerTwo: BUM!");
                Cannon();
            }
        }
        else 
            Reload();
    }
    void OnCollisionEnter(Collision collision)
    {
        if(powerUp.Equals(false) && collision.gameObject.tag.Equals("PowerUp"))
        {
            powerUp = true;
            reloadTime /= 3;
        }
    }
    void Buff()
    {
        buffTime -= Time.deltaTime;
        if (buffTime < 0)
        {
            powerUp = false;
            reloadTime *= 3;
        }
    }
}
