using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

public class Tank
{
    public int Speed => _speed;
    public int Rotation => _rotation;
    public float ReloadTime => _reloadTime;

    private readonly int _speed, _rotation;
    private readonly float _reloadTime;
    [SerializeField] GameObject TankType;

    public Tank(int speed, int rotation, float reloadTime)
    {
        _speed = speed;
        _rotation = rotation;
        _reloadTime = reloadTime;
    }
    public void SetType(GameObject tankType)
    {
        TankType = tankType;
    }
}
