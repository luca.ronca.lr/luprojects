using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public string Name => _name;
    public Tank Tank => _tank;

    private readonly string _name;
    private readonly Tank _tank;
    public Player(string name, Tank tank)
    {
        _name = name;
        _tank = tank; 
    }
}
