using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GUIBattleGroundManager : MonoBehaviour
{
    [SerializeField] private TMP_Text  playerOneName, playerTwoName;
    [SerializeField] private TMP_Text timer;
    [SerializeField] private float startTime;
    [SerializeField] private TMP_Text winner;
    [SerializeField] private GameObject endGame, mainCamera, pOneView, pTwoView;
    private float currentTime;

    private void Start()
    {
        playerOneName.text = PlayerPrefs.GetString("PlayerOneName");
        playerTwoName.text = PlayerPrefs.GetString("PlayerTwoName");
        currentTime = startTime;
        PlayerPrefs.SetString("Winner", "X");
        endGame.SetActive(false);
    }
    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape)) 
            Application.Quit();
        if (Input.GetKeyUp(KeyCode.C))
            SetCamera(mainCamera, pOneView, pTwoView);
        if(PlayerPrefs.GetString("Winner") == "X")
            Timer();
        else
            EndGame();
    }
    void Timer()
    {
        currentTime -= Time.deltaTime;

        if (currentTime < 0)
        {
            currentTime = 0;
            Debug.Log("End game");
            EndGame();
        }

        TimeSpan timeSpan = TimeSpan.FromSeconds(currentTime);
        string timeString = string.Format("{0:D1}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
        timer.text = timeString;
    }
    public void MenuButton()
    {
        SceneManager.LoadScene("SimpleMainMenu");
    }
    public void ExitButton()
    {
        Application.Quit();
    }
    public void SetCamera(GameObject camera1, GameObject camera2, GameObject camera3)
    {
        if (camera1.active == true)
        {
            camera1.SetActive(false);
            camera2.SetActive(true);
            camera3.SetActive(true);
        }
        else
        {
            camera1.SetActive(true);
            camera2.SetActive(false);
            camera3.SetActive(false);
        }
    }
    void EndGame()
    {
        winner.text = PlayerPrefs.GetString("Winner") + "\nWon";
        endGame.SetActive(true);
    }
}
