using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public static float timeRemaining = 10.0f;
    [SerializeField] private TextMeshPro timeText;
    // Start is called before the first frame update
    void Start()
    {
        timeText.text = timeRemaining.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            timeText.text = timeRemaining.ToString();
        }

    }
}
