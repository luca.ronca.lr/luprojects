using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEx : MonoBehaviour
{
    private Vector3 jumpForce = new Vector3(0, 5.0f, 0);
    private bool jumpStarted = false;
    public GameObject timerGo;
    void Start()
    {
        
    }
    void Update()
    {
        if(Input.GetButton("Jump") && !jumpStarted)
        {
            this.transform.GetComponent<Rigidbody>().AddForce(jumpForce);
            jumpStarted = true;
            Timer.timeRemaining = 2.0f;

            
        }
        if (Input.GetButton("Jump") && Timer.timeRemaining == 0.0f && jumpStarted)
        {
            this.transform.GetComponent<Rigidbody>().AddForce(jumpForce);
        }
    }
}
