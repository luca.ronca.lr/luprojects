using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreLights : MonoBehaviour
{
    [SerializeField] private int scoreRef;
    [SerializeField] private Material lightOn;
    private bool changed = false;
    private MeshRenderer rend;
    void Start()
    {
        rend = GetComponent<MeshRenderer>();
        rend.enabled = true;
    }
    void Update()
    {
        if (!changed)
        {
            if(Player.score >= scoreRef)
            {
                TurnOn();
                changed = true;
            }
        }
    }
    private void TurnOn()
        => rend.sharedMaterial = lightOn;
}
