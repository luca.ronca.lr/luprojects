using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TifoAnimator : MonoBehaviour
{
    private Animator animator;
    public static bool point;
    private float currentTime;
    void Start()
    {
        animator = GetComponent<Animator>();
        point = false;
        currentTime = Random.Range(0.0f, 1.5f);
    }
    void Update()
    {
        if (currentTime < 0)
            animator.SetBool("Point", point);
        else
            Timer();
    }
    private void Timer()
    {
        currentTime -= Time.deltaTime;
    }
}
