using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource cheerFront, cheerBack, disFront, disBack;

    public static AudioSource cheerFrontSource, cheerBackSource, disFrontSource, disBackSource;
    void Start()
    {
        cheerFrontSource = cheerFront;
        cheerBackSource = cheerBack;
        disFrontSource = disFront;
        disBackSource = disBack;
    }
}
