using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tifo : MonoBehaviour
{
    [SerializeField] private GameObject[] tifosi;
    [SerializeField] private Transform[] posti;
    void Start()
    {
        switch(Player.score)
        {
            case 0:
                SpawnTifosi(Random.Range(1, 2));
                break;
            case 1:
                SpawnTifosi(Random.Range(3, 5));
                break;
            case 2:
                SpawnTifosi(Random.Range(6, 9));
                break;
            case 3:
                SpawnTifosi(Random.Range(10,12));
                break;
            default: 
                break;
        }
    }
    void Update()
    {
        
    }
    private void SpawnTifosi(int maxTifosi)
    {
        int n;
        List<int> indici = new List<int>();
        indici.Add(Random.Range(0, 11));
        for (int i = 1; i < maxTifosi; i++)
        {
            do
                n = Random.Range(0, 11);
            while (indici.Contains(n));
            indici.Add(n);
        }
        foreach(int i in indici)
        {
            Instantiate(tifosi[Random.Range(0, 5)], posti[i]);
        }
    }
}
