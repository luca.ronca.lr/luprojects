using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] private Transform[] positions;
    [SerializeField] private float timeToEnd;
    [SerializeField] private GameObject tutorialPanel, gameEndPanel, buttonCollider, toccatoreDx, toccatoreSx;
    [SerializeField] private TMP_Text title, timeStats, errorsStats, errorsRef;
    
    public static int score = 0, errors = 0;
    public static float roundTime = 61, totalTime = 0;
    public static bool gameover, timeout = false, startGame = true;
    void Start()
    {
        this.gameObject.transform.position = positions[score].position;
        gameover = false;
        if (score > 0)
        {
            tutorialPanel.SetActive(false);
            buttonCollider.GetComponent<BoxCollider>().enabled = false;
            toccatoreDx.GetComponent<BoxCollider>().enabled = false;
            toccatoreSx.GetComponent<BoxCollider>().enabled = false;
        }
        errorsStats.text = String.Empty;    
        gameEndPanel.SetActive(false);     
    }
    void Update()
    {
        if (!gameover && startGame)
            TotalTimeStats();
        if(errors == 3 || timeout)
            gameover = true;
        
        if(gameover && score < 4 && errors < 3 && !timeout)
        {
            EndRound();
            return;
        } 
        if(gameover)
            EndGame();
    }
    public static void AutoChangeScene()
    {
        if(score < 4)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public static float roundTimer(float currentTime)
    {
        currentTime -= Time.deltaTime;
        if(currentTime < 0)
            timeout = true;
        return currentTime;
    }
    private void EndRound()
    {
        timeToEnd -= Time.deltaTime;
        if (timeToEnd < 0)
            AutoChangeScene();
    }
    private void EndGame()
    {
        gameEndPanel.SetActive(true);
        buttonCollider.SetActive(true);
        if (errors == 3 || timeout)
            title.text = "You Lose";
        if(score == 4)
            title.text = "You Win";
        TimeSpan timeSpan = TimeSpan.FromSeconds(totalTime);
        timeStats.text = string.Format("0{0:D1} : {1:D2}", timeSpan.Minutes, timeSpan.Seconds);
        errorsStats.text = errorsRef.text;
        buttonCollider.GetComponent<BoxCollider>().enabled = true;
        toccatoreDx.GetComponent<BoxCollider>().enabled = true;
        toccatoreSx.GetComponent<BoxCollider>().enabled = true;
    }
    private void TotalTimeStats()
    {
        totalTime += Time.deltaTime;
    }
}
