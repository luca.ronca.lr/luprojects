using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Traiettoria : MonoBehaviour
{
    [SerializeField] private int id;
    [SerializeField] private GameObject plane;
    public Transform[] waypoints; 
    public float speed; 

    private int currentWaypoint;
    private bool follow;

    private void Start()
    {
        if(id == Player.score)
            plane.GetComponent<MeshRenderer>().enabled = true;
        follow = false;
    }
    private void Update()
    {
        if (follow)
            followPath();
    }
    private void followPath()
    {
        if (currentWaypoint < waypoints.Length)
        {
            Vector3 targetPosition = waypoints[currentWaypoint].position;
            float step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
            if (transform.position == targetPosition)
                currentWaypoint++;
            if(currentWaypoint == waypoints.Length-1)
            {
                follow = false;
                this.GetComponent<Rigidbody>().useGravity = true;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (id != Player.score)
            return;
        if (other.gameObject.CompareTag("StartPath"))
        {
            follow = true;
            currentWaypoint = 0;
            this.GetComponent<Rigidbody>().useGravity = false;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
            this.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
    }
}
