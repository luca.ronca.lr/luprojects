using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class ButtonCollider : MonoBehaviour
{
    [SerializeField] private GameObject tutorialPanel, toccatoreDx, toccatoreSx;
    private void Start()
    {
        if(Player.score == 0)
            Player.startGame = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Neutro"))
        {
            if (Player.errors == 3)
            {
                Player.score = 0;
                Player.errors = 0;
                Player.totalTime = 0;
                Player.AutoChangeScene();
            }
            if (Player.score == 0)
            {
                Player.startGame = true;
                tutorialPanel.SetActive(false);
                this.GetComponent<BoxCollider>().enabled = false;
                toccatoreDx.GetComponent<BoxCollider>().enabled = false;
                toccatoreSx.GetComponent<BoxCollider>().enabled = false;
            }
        }
        if (Player.gameover)
        {
            Player.score = 0;
            Player.errors = 0;
            Player.totalTime = 0;
            Player.AutoChangeScene();
        }
    }
}