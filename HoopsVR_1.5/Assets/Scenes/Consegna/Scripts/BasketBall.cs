using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class BasketBall: MonoBehaviour
{
    private bool start = false, usable = true;
    private void OnCollisionEnter(Collision collision)
    {
        this.GetComponent<AudioSource>().Play();
        if (!usable)
            return;

        if (!collision.gameObject.CompareTag("Neutro") && !collision.gameObject.CompareTag("StartPath"))
        {
            Player.errors++;
            this.GetComponent<XRGrabInteractable>().enabled = false;
            usable = false;
            AudioManager.disFrontSource.Play();
            AudioManager.disBackSource.Play();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!usable)
            return;

        if (!start)
        {
            if(other.gameObject.CompareTag("Start"))
                start = true;
        }
        else
        {
            if(other.gameObject.CompareTag("End"))
            {
                Player.score++;
                Player.gameover = true;
                TifoAnimator.point = true;
                this.GetComponent<XRGrabInteractable>().enabled = false;
                usable = false;
                AudioManager.cheerFrontSource.Play();
                AudioManager.cheerBackSource.Play();
            }
        }
    }
}
