using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SegnaPunti : MonoBehaviour
{
    [SerializeField] private TMP_Text timer, errorsText;
    private int errors = 0;
    private float currentTime;
    private bool updated;

    void Start()
    {
        currentTime = Player.roundTime;
        errorsText.text = string.Empty;
    }
    void Update()
    {
        if(currentTime > 0 && !Player.gameover && Player.startGame)
        {
            currentTime = Player.roundTimer(currentTime);
            TimeSpan timeSpan = TimeSpan.FromSeconds(currentTime);
            timer.text = string.Format("0{0:D1} : {1:D2}", timeSpan.Minutes, timeSpan.Seconds);
            if (currentTime < 10)
                colorText(currentTime);
        }
        if (errors < Player.errors) 
        {
            errorsText.text += "x ";
            errors ++;
        }
    }
    private void colorText(float currentTime)
    {
        int tempo = Mathf.CeilToInt(currentTime);
        if (tempo % 2 == 0) 
            timer.color = Color.red;
        else
            timer.color = Color.white;
    }
}
