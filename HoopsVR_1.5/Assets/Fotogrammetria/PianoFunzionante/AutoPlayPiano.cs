using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlayPiano : MonoBehaviour
{
    [SerializeField] Transform coord;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Spartito"))
        {
            other.transform.position = coord.position;
            other.transform.rotation = coord.rotation;
            other.GetComponent<AudioSource>().Play();
        }
            
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Spartito"))
        {
            other.GetComponent<AudioSource>().Stop();
        }
            
    }
}
