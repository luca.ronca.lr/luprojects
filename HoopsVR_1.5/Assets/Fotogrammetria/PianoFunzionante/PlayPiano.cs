using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPiano : MonoBehaviour
{
    private AudioSource keySound;
    private void Start()
        => keySound = GetComponent<AudioSource>();
    private void OnCollisionEnter(Collision collision)
        => keySound.Play();
}
