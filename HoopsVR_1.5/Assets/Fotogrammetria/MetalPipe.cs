using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalPipe : MonoBehaviour
{
    int cade = 0;
    private void Start()
    {
        cade = 0;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Spartito"))
        {
            GetComponent<AudioSource>().Play();
            cade++;
            PlayerPrefs.SetInt("CadutaTubi", cade);
        }
    }
}
