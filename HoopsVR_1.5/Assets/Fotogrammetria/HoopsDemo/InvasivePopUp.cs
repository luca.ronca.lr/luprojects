using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InvasivePopUp : MonoBehaviour
{
   
    [SerializeField] private GameObject popUpCanvas;
    [SerializeField] private Transform camera;
    [SerializeField] private float distanceFromCamera;

    public static bool gameover;
    private void Start()
    {
        gameover = false;
    }
    private void Update()
    {
        GetComponent<BoxCollider>().enabled = gameover;

        //PopUp Position
        popUpCanvas.gameObject.SetActive(gameover);
        transform.position = camera.position + new Vector3(camera.forward.x, 0, camera.forward.z).normalized * distanceFromCamera;

        //PopUp Orientation
        transform.LookAt(new Vector3(camera.position.x, popUpCanvas.transform.position.y, camera.position.z));
        transform.forward *= -1;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Palla"))
            SceneManager.LoadScene("Fotogrammetria");
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Palla"))
            SceneManager.LoadScene("Fotogrammetria");
    }
}
