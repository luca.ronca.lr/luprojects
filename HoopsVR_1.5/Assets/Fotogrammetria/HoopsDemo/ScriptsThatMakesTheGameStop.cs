using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScriptsThatMakesTheGameStop : MonoBehaviour
{
    [SerializeField] TMP_Text eroors;
    float timer;
    private void Start()
    {
        timer = 0;
    }
    // Update is called once per frame
    void Update()
    {
        Check();
    }
    private void Check()
    {
        int punto = PlayerPrefs.GetInt("PUNTO");
        if (eroors.text.Equals("X X X ") || Tabellone.currentTime <= 0)
        {
            InvasivePopUp.gameover = true;
        }
        else if (punto == 3 && eroors.text.Equals("X "))
        {
            InvasivePopUp.gameover = true;
        }
        else if (punto == 2 && eroors.text.Equals("X X "))
        {
            InvasivePopUp.gameover = true;
        }
        else if (punto == 4)
        {
            InvasivePopUp.gameover = true;
        }
    }
}
