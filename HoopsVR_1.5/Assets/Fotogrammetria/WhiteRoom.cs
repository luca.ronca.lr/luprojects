using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WhiteRoom : MonoBehaviour
{
    public static bool visible = false;
    private float currentTime = 5.0f;
    private void Start()
    {
        visible = false;
    }
    void Update()
    {
        if (visible)
        {
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<BoxCollider>().enabled = true;
            Timer();
        }
    }
    private void Timer()
    {
        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
