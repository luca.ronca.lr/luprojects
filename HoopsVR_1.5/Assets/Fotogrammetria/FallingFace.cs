using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;

public class FallingFace : MonoBehaviour
{   
    [SerializeField] private float speed;
    [SerializeField] private Transform target;
    public static int noises = 0;
    public static bool fallen = false;
    private float distanceIniziale;
    public bool cadi = false;
    private void Start()
    {
        distanceIniziale = Vector3.Distance(transform.position, target.position);
        cadi = false;
        PlayerPrefs.SetInt("CadutaTubi", 0);
        GetComponent<AudioSource>().volume = 0;
        GetComponent<AudioSource>().Play();
    }
    void Update()
    {
        int reference = PlayerPrefs.GetInt("CadutaTubi");
        switch (reference)
        {
            case 1: cadi = true; speed = .25f; break;
            case 2: speed = .75f; break;
            case 3: speed = 5.5f;  break;
        }
        if (cadi)
        {
            AlzaVolume();
            GetComponent<PositionConstraint>().enabled = false;
            Falling();
        }
    }
    private void Falling()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        if(transform.position == target.position)
        {
            fallen = true;
            GetComponent<AudioSource>().Stop();
            WhiteRoom.visible = true;
            Destroy(gameObject);
        }
    }
    private void AlzaVolume()
    {
        float distanceAttuale = Vector3.Distance(this.transform.position, target.position);
        
        GetComponent<AudioSource>().volume = 1 - (distanceAttuale/distanceIniziale);
        Debug.Log(distanceIniziale / distanceAttuale);
    }
}
