using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraiettoriaFisica : MonoBehaviour
{
    [SerializeField] private Transform maxHeight;
    private Vector3 initialPoint, finalPoint;
    private float distance, speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Palla"))
            initialPoint = other.gameObject.transform.position;
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Palla"))
        {
            finalPoint = other.gameObject.transform.position;
            distance = Vector3.Distance(initialPoint, finalPoint);
            speed = Mathf.Sqrt(distance * Physics.gravity.magnitude / Mathf.Sin(2f * Mathf.Deg2Rad * GetLaunchAngle(distance)));
            other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            Launch(speed, GetLaunchAngle(distance), other.gameObject.GetComponent<Rigidbody>());
        }
    }
    private float GetLaunchAngle(float distance)
    {
        float launchAngle = Mathf.Atan((Mathf.Pow(distance, 2f) * Physics.gravity.magnitude) / (2f * maxHeight.transform.position.y));
        launchAngle = Mathf.Rad2Deg * launchAngle;
        return launchAngle;
    }
    private void Launch(float initialSpeed, float launchAngle, Rigidbody rb)
    {
        float vx = initialSpeed * Mathf.Cos(Mathf.Deg2Rad * launchAngle);
        float vy = initialSpeed * Mathf.Sin(Mathf.Deg2Rad * launchAngle);

        rb.velocity = new Vector3(vx, vy, 0f);
    }
}
