using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Windows;

public class TiroAccettabile : MonoBehaviour
{
    public Transform[] waypoints = new Transform[4];
    public float speed;
    Vector3 continuativo;

    private int currentWaypoint;
    private bool follow;
    private bool isBased = false;

    private void Start()
    {
        follow = false;
    }
    private void Update()
    {
        if (follow && !isBased)
            followPath();
    }
    private void followPath()
    {
        if (currentWaypoint < waypoints.Length)
        {
            float targetPosition = waypoints[currentWaypoint].position.y;

            continuativo = new Vector3(transform.position.x, targetPosition, transform.position.z);

            float step = speed * Time.deltaTime;

            // DA USARE CON IL GATTO nonono = Vector3.Cross(transform.position, targetPosition);

            transform.position = Vector3.MoveTowards(transform.position, continuativo, step);

            if (transform.position.y <= targetPosition + 0.25 || transform.position.y >= targetPosition -0.25)
                currentWaypoint++; Debug.Log("Shakerando");
            if (currentWaypoint == waypoints.Length - 1)
            {
                Debug.Log("Sto indipendente");
                follow = false;
                //this.GetComponent<Rigidbody>().useGravity = true; Debug.Log("Sto ignorando lo script");
                isBased = true;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Throw"))
        {
            follow = true;
            currentWaypoint = 0;
            //this.GetComponent<Rigidbody>().useGravity = false;
            //this.GetComponent<Rigidbody>().velocity = new Vector3(-1,0,-1);
            //this.GetComponent<Rigidbody>().angularVelocity = new Vector3(speed, 0, speed);
        }
    }
}
