using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Unity.VisualScripting;

public class Tabellone : MonoBehaviour
{
    static public float currentTime;
    private float duration = 61;
    int point;

    [SerializeField] TMP_Text timer;

    void Start()
    {
        currentTime = duration;
        point = PlayerPrefs.GetInt("PUNTO");
        timer.color = Color.white;
    }


    void Update()
    {
        if (currentTime > 0)
            FunzioneCheFaScorrereIlTempo();
    }

    #region Tiemr
    void FunzioneCheFaScorrereIlTempo()
    {
        currentTime -= Time.deltaTime;
        if (currentTime < 10)
            FunzioneCheCambiaColoreDelTimerGliUltimiCinquePerDueSecondiRimanenti();
        TimeSpan timeSpan = TimeSpan.FromSeconds(currentTime);
        timer.text = string.Format("{0:D1}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
    }
    #endregion
    #region soplo perche cos'
    void FunzioneCheCambiaColoreDelTimerGliUltimiCinquePerDueSecondiRimanenti()
    {
        int tempuo = Mathf.CeilToInt(currentTime);
        if (tempuo % 2 == 0)
            timer.color = Color.red;
        else
            timer.color = Color.white;
    }
    #endregion
}
