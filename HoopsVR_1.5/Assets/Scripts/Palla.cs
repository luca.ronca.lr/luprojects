using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Palla : MonoBehaviour
{
    private bool sfracellatoATerra = false;
    private bool canestro = false;
    public int point = 0;
    public int initialPoint = 0;

    [SerializeField] private TMP_Text eroorText;
    private void Start()
    {
        PlayerPrefs.GetInt("PUNTO");
        initialPoint = point;
        eroorText.text = string.Empty;
    }

    private void Update()
    {
        GameOver();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Start")
        {
            canestro = true;
            Debug.Log(canestro);
            return;
        }
        if (other.gameObject.tag == "End" && canestro == true)
        {
            point++;
            sfracellatoATerra = true;
            PlayerPrefs.SetInt("PUNTO", point);
            Debug.Log(point);
            return;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag != "Neutro" && collision.gameObject.tag != "End" 
            && collision.gameObject.tag != "Start" && collision.gameObject.tag != "Palla" 
            && collision.gameObject.tag != "StartPath" && collision.gameObject.tag != "Throw")
        {
            canestro = false;
            if (sfracellatoATerra)
            {
                return;
            }
            if(eroorText.text.Length >= 6)
            {
                return;
            }
            eroorText.text += "X ";
            sfracellatoATerra = true;
        }
    }
    public void GameOver()
    {
        if(eroorText.text.Equals("X X X "))
        {
            Debug.Log("Hai perso, sfigato");
        }
    }
}
