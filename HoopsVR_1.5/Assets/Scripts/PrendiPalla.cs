using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.Animations;

public class PrendiPalla : MonoBehaviour
{
    [SerializeField] private GameObject Palla;
    private void OnCollisionEnter(Collision collision)
    {
       
        Debug.Log("Collisione");
        if (collision.gameObject.tag == "Palla")
        {
            Debug.Log("Palla");
            if (Input.GetAxis("XRI_Right_Grip") > 0.25f)
            {
                Debug.Log("OCCHEI");
                Palla.GetComponent<PositionConstraint>().enabled = true;
            }
            else
            {
                Palla.GetComponent<PositionConstraint>().enabled = false;
            }
        }
    }
}
