using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptThatMemorizeThePositionOnThePlaneOnceTheBallisReleasedInItalian : MonoBehaviour
{
    public static Vector2 positionMemorized;
    static public Vector2 piero;

    private void Start()
    {
        positionMemorized = Vector2.zero;
        piero = Vector2.zero;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Palla")
        {
            if(positionMemorized == Vector2.zero && piero == Vector2.zero)
            {
                positionMemorized = new Vector2(other.transform.position.x, other.transform.position.z);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Palla")
        {
            if (positionMemorized != Vector2.zero && piero == Vector2.zero)
            {
                piero = new Vector2(other.transform.position.x, other.transform.position.z);
            }
        }
    }
}