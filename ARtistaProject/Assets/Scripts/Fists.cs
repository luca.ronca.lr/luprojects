using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Fists : MonoBehaviour
{
    [SerializeField] private InputActionProperty GrabDx, GrabSx;
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            if(GrabDx.action.ReadValue<float>() == 1 && GrabSx.action.ReadValue<float>() == 1)
                GetComponent<AudioSource>().Play();
        }
            
    }
}
