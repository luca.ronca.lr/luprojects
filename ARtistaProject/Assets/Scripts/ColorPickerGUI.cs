using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ColorPickerGUI : MonoBehaviour
{
    [SerializeField] private Material[] colors = new Material[8];
    [SerializeField] private Button[] buttons = new Button[8];
    [SerializeField] private Slider[] sliders = new Slider [4];
    [SerializeField] private TMP_Text[] sliderValue = new TMP_Text[4];
    public Image result, colorPickerImage;
    private int selectedColor = 0;
    private void Start()
    {
        int i = 0;
        foreach (var b in buttons)
        {
            b.GetComponent<Image>().color = colors[i].color;
            i++;
        }
        SliderUpdate();
        InfoUpdate();
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            colorPick();
        InfoUpdate();
    }
    private void SliderUpdate()
    {
        sliders[0].value = colors[selectedColor].color.r;
        sliders[1].value = colors[selectedColor].color.g;
        sliders[2].value = colors[selectedColor].color.b;
        sliders[3].value = colors[selectedColor].color.a;
    }
    private void InfoUpdate()
    {
        for (int i = 0; i < 4; i++)
        {
            float value = sliders[i].value * 255f;
            sliderValue[i].text = value.ToString("F0");
        }
        result.color = new Color(sliders[0].value, sliders[1].value, sliders[2].value, sliders[3].value);
        //buttons[selectedColor].GetComponent<Image>().color = result.color;
    }
    public void ColorSelectButton(int buttonNumber)
    {
        selectedColor = buttonNumber;
        SliderUpdate();
        InfoUpdate();
    }
    public void ConfirmButton()
    {
        colors[selectedColor].color = new Color(sliders[0].value, sliders[1].value, sliders[2].value, sliders[3].value);
        buttons[selectedColor].GetComponent<Image>().color = result.color;
    }
    public void ValueChange()
    {
        InfoUpdate();
    }
    private void colorPick()
    {
        Vector2 mousePosition = Input.mousePosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(colorPickerImage.rectTransform, mousePosition, Camera.main, out Vector2 localPosition);

        if (localPosition.x >= -colorPickerImage.rectTransform.rect.width / 2 && localPosition.x <= colorPickerImage.rectTransform.rect.width / 2 && localPosition.y >= -colorPickerImage.rectTransform.rect.height / 2 && localPosition.y <= colorPickerImage.rectTransform.rect.height / 2)
        {
            Texture2D texture = (Texture2D)colorPickerImage.mainTexture;
            float textureCoordX = (localPosition.x + colorPickerImage.rectTransform.rect.width / 2) / colorPickerImage.rectTransform.rect.width;
            float textureCoordY = (localPosition.y + colorPickerImage.rectTransform.rect.height / 2) / colorPickerImage.rectTransform.rect.height;

            Color pixelColor = texture.GetPixelBilinear(Mathf.Clamp01(textureCoordX), Mathf.Clamp01(textureCoordY));
            sliders[0].value = pixelColor.r;
            sliders[1].value = pixelColor.g;
            sliders[2].value = pixelColor.b;
            sliders[3].value = pixelColor.a;    
            InfoUpdate();
            result.color = pixelColor;
        }
    }
}