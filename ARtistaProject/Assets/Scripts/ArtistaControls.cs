using UnityEngine;

public class ArtistaControls : MonoBehaviour
{
    [SerializeField] private GameObject artistaSet, artistaGUI, eraser, loader;
    [SerializeField] private GameObject[] rooms = new GameObject[2];
    [SerializeField] private Transform[] roomsTransform = new Transform[2];
    static public int selectedRoom;
    void Start()
    {
        artistaSet.SetActive(false);
        artistaGUI.SetActive(false);
        eraser.SetActive(false);
        loader.SetActive(false);
        selectedRoom = PlayerPrefs.GetInt("ARtistaRoom");
        changeActiveRoom();
    }
    void Update()
    {
        if (Input.GetButtonDown("OculusA"))
            artistaSet.SetActive(!artistaSet.activeSelf);
        if (Input.GetButtonDown("OculusX"))
            ChangeRoom();
        if (Input.GetButtonDown("OculusMenu"))
        {
            artistaGUI.SetActive(!artistaGUI.activeSelf);
            eraser.SetActive(false);
            loader.SetActive(false);
        }
    }
    private void changeActiveRoom() => Instantiate(rooms[selectedRoom], roomsTransform[selectedRoom]);
    private void ChangeRoom()
    {
        if (selectedRoom < rooms.Length - 1)
            selectedRoom++;
        else
            selectedRoom = 0;
        changeActiveRoom();
    }
}
