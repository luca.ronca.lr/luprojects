using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomDestroyer: MonoBehaviour
{
    [SerializeField] private int roomId;
    void Update()
    {
        switch(SceneManager.GetActiveScene().name)
        {
            case "OpenMenu":
                if (roomId != ArtistaSetupGUI.selectedRoom)
                    Destroy(this.gameObject);
                break;
            case "MainScene":
                if (roomId != ArtistaControls.selectedRoom)
                    Destroy(this.gameObject);
                break;
            default:
                break;
        }
    }
}