using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ArtistaSetupGUI : MonoBehaviour
{
    [SerializeField] private string mainSceneName;
    [SerializeField] private TMP_InputField nameField;
    [SerializeField] private GameObject[] rooms = new GameObject[2];
    [SerializeField] private Transform[] roomsTransform = new Transform[2];
    [SerializeField] private Button[] buttons = new Button[2];
    static public int selectedRoom;
    void Start()
    {
        if (string.IsNullOrEmpty(PlayerPrefs.GetString("ARtistaName")))
            PlayerPrefs.SetString("ARtistaName", "ARtista");
        PlayerPrefs.SetInt("ARtistaRoom", 0);
        changeActiveRoom();
    }
    private void changeActiveRoom()
    {
        Instantiate(rooms[selectedRoom], roomsTransform[selectedRoom]);
        disableButtons();
    }
    private void disableButtons()
    {
        foreach(Button b in buttons)
        {
            b.interactable = true;
        }
        buttons[selectedRoom].interactable = false;
    }
    public void RoomButton(int nRoom)
    {
        selectedRoom = nRoom;
        changeActiveRoom();
    } 
    public void StartButton()
    {
        PlayerPrefs.SetString("ARtistaName", nameField.text);
        PlayerPrefs.SetInt("ARtistaRoom", selectedRoom);
        //Debug.Log(PlayerPrefs.GetString("ARtistaName") + " - " + PlayerPrefs.GetInt("ARtistaRoom"));
        SceneManager.LoadScene(mainSceneName);
    }
}
