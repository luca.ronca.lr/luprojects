using UnityEngine;

public class BrushColorChange : MonoBehaviour
{
    [SerializeField] private GameObject visibleTip;
    private Renderer newColor, target;
    private Color dilution;
    private void Start()
    {
        target = this.GetComponent<Renderer>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Color"))
        {
            newColor = other.gameObject.GetComponent<Renderer>();
            target.material.color = newColor.material.color;
            visibleTip.GetComponent<Renderer>().material.color = target.material.color;
        }
        if (other.gameObject.CompareTag("Water"))
        {
            dilution = target.material.color;
            if (dilution.a > 0.4f)
                dilution.a -= 0.2f;
            else
                dilution.a = 0.1f;
            target.material.color = dilution;
            visibleTip.GetComponent<Renderer>().material.color = dilution;
        }
    }
}
