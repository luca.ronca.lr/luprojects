using System.IO;
using TMPro;
using UnityEngine;

public class SaveDeleteGUI : MonoBehaviour
{
    [SerializeField] private GameObject eraser, loader, cameraOBJ;
    [SerializeField] private Camera captureCamera;

    [SerializeField] private TMP_InputField paintNameField;
    private string folderPath, filePath, fileName;

    private void Start()
    {
        cameraOBJ.SetActive(false);
        /*PC
        folderPath = System.IO.Path.Combine(Application.dataPath, );
        if (!System.IO.Directory.Exists(folderPath))
            System.IO.Directory.CreateDirectory(folderPath);
        //filePath = System.IO.Path.Combine(folderPath, fileName);
        */
        folderPath = Path.Combine(Application.persistentDataPath, "VRtista - Paints");
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);
    }
    private void FilePathController()
    {
        if (string.IsNullOrEmpty(paintNameField.text))
        {
            Debug.Log("Errore: Nessun nome");
            return;
        }
        fileName = paintNameField.text + ".png";
        filePath = Path.Combine(folderPath, fileName);
    }
    private void Saving()
    {
        FilePathController();
        if (!File.Exists(filePath))
        {
            cameraOBJ.SetActive(true);
            CaptureScreenshot();
            cameraOBJ.SetActive(false);
        }
        else
        {
            Debug.Log("Dipinto esistente");
            File.Delete(filePath);
            cameraOBJ.SetActive(true);
            CaptureScreenshot();
            filePath = Path.Combine(folderPath, fileName);
            cameraOBJ.SetActive(false);
        }
    }
    private void CaptureScreenshot()
    {
        int width = 1920 /*captureCamera.pixelWidth*/;
        int height = 1080 /*captureCamera.pixelHeight*/;
        RenderTexture renderTexture = new RenderTexture(width, height, 24);
        Texture2D screenshot = new Texture2D(width, height, TextureFormat.RGB24, false);

        captureCamera.targetTexture = renderTexture;
        captureCamera.Render();

        RenderTexture.active = renderTexture;
        screenshot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        screenshot.Apply();

        byte[] bytes = screenshot.EncodeToPNG();
        File.WriteAllBytes(filePath, bytes);
        Debug.Log("Save complete - " + filePath);

        RenderTexture.active = null;
        captureCamera.targetTexture = null;

        Destroy(renderTexture);
    }
    private void Loading()
    {
        FilePathController();
        if (!File.Exists(filePath))
        {
            Debug.Log("Dipinto inesistente");
            return;
        }
        byte[] imageData = File.ReadAllBytes(filePath);
        Texture2D texture = new Texture2D(1920, 1080);
        texture.LoadImage(imageData);
        loader.GetComponent<Renderer>().material.mainTexture = texture;
        loader.SetActive(true);
    }
    public void SaveButton()
    {
        Debug.Log(paintNameField.text + " - " + PlayerPrefs.GetString("ARtistaName"));
        Saving();
    }
    public void LoadButton()
    {
        Debug.Log("Loading " + paintNameField.text);
        eraser.SetActive(false);
        Loading();
    }
    public void DeleteButton() => eraser.SetActive(true);
}
