using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorDilution : MonoBehaviour
{
    private Color dilution;
    private Renderer target;
    private void Start()
    {
        target = this.GetComponent<Renderer>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Water"))
        {
            dilution = target.material.color;
            Debug.Log(dilution.a);
            dilution.a -= 0.2f;
            Debug.Log(dilution.a);
            target.material.color = dilution;
        }
    }
}
