using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ShipAnimation : MonoBehaviour
{
    private Animator shipAnimator;
    void Start()
    {
        shipAnimator = GetComponent<Animator>();
    }
    void Update()
    {
        shipAnimator.SetFloat("Turning", Input.GetAxis("Horizontal"));
    }
}
