using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCommands : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private GameObject starShip, bullet;
    [SerializeField] private Transform cannon, altCannonDx, altCannonSx;
    [SerializeField] private float reloadingTime;
    
    private bool bulletLoad = true;
    private float reloading;
    private void Start()
    {
        movementSpeed = PlayerPrefs.GetFloat("MovementSpeed");

        reloading = reloadingTime;
    }
    void Update()
    {
        if (Player.gameOver || Player.win)
            return;

        HorizontalMovement(movementSpeed);
        if(Player.powerUpType != 0)
        {
            if (bulletLoad)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    if (Player.powerUpType == 1)
                    {
                        Firing(bullet, bulletSpeed, cannon);
                        AudioPlayer.laserSound.Play();
                    }
                    else
                    {
                        Firing(bullet, bulletSpeed, altCannonSx, altCannonDx);
                        AudioPlayer.laserSound.Play();
                    }
                }
            }
            else
                Reload(reloadingTime);
        }
    }
    private void HorizontalMovement(float speed)
    {
        float move = Input.GetAxis("Horizontal");
        Vector2 force = new Vector2(move, 0f) * speed;
        GetComponent<Rigidbody>().velocity = force;
    }
    private void Firing(GameObject bulletPrefab, float speed, Transform spawnPoint)
    {
        var bullet = Instantiate(bulletPrefab, spawnPoint.position, bulletPrefab.transform.rotation);
        bullet.GetComponent<Rigidbody>().velocity = spawnPoint.forward * speed;
        bulletLoad = false;
    }
    private void Firing(GameObject bulletPrefab, float speed, Transform spawnPointDx,Transform spawnPointSx)
    {
        //Dx
        var bulletDx = Instantiate(bulletPrefab, spawnPointDx.position, bulletPrefab.transform.rotation);
        bulletDx.GetComponent<Rigidbody>().velocity = spawnPointDx.forward * speed;
        //Sx
        var bulletSx = Instantiate(bulletPrefab, spawnPointSx.position, bulletPrefab.transform.rotation);
        bulletSx.GetComponent<Rigidbody>().velocity = spawnPointSx.forward * speed;

        bulletLoad = false;
    }
    private void Reload(float time)
    {
        reloading -= Time.deltaTime;
        if(reloading <= 0)
        {
            reloading = time;
            bulletLoad = true;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Asteroid") || collision.gameObject.CompareTag("SpecialAsteroid"))
        {
            Player.gameOver = true;
            ParticleEffects.engineInstance.Stop();
            Destroy(starShip);
        }
    }
}
