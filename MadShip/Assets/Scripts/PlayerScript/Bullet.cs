using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class Bullet : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(!collision.gameObject.CompareTag("Ship"))
        {
            if (collision.gameObject.CompareTag("SpecialAsteroid"))
            {
                Player.score += 1;
                Debug.Log(Player.score);
            }
            Destroy(gameObject);
        }
    }
}
