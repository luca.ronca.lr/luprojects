using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] private bool buff;
    [SerializeField] private float duration;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ship"))
        {
            Player.powerUpRemainigTime = duration;
            if (buff)
                Player.powerUpType = 2;
            else
                Player.powerUpType = 0;
        }
        if(collision.gameObject.CompareTag("Ship") || collision.gameObject.CompareTag("DownCollider"))
        {
            Destroy(this.gameObject);
        }
    }
}
