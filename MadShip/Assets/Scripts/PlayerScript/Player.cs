using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float cameraSpeed, levelDuration, timeToFinish;

    public static int score, specialAsteroidNumber, powerUpType;
    public static float remainingTime, remainingToFinish, powerUpRemainigTime;
    public static bool gameOver, win;
    private void Start()
    {
        cameraSpeed = PlayerPrefs.GetFloat("CameraSpeed");

        Time.timeScale = 1f;
        score = 0;
        specialAsteroidNumber = 0;
        remainingTime = levelDuration;
        remainingToFinish = timeToFinish;
        gameOver = false;
        win = false;
        powerUpType = 1;
    }
    void Update()
    {
        if (!gameOver && !win)
            cameraForward(cameraSpeed);
        LevelTimer();
        if(powerUpType != 1)
            powerUpTimer();
        if (win || gameOver)
            FinishingTimer();
    }
    void cameraForward(float speed)
        => this.transform.Translate(0, 0, speed);
    private void LevelTimer()
    {
        remainingTime -= Time.deltaTime;
        if(remainingTime <= 0)
            win = true;
    }
    private void FinishingTimer()
    {
        remainingToFinish -= Time.deltaTime;
        if(remainingToFinish <= 0)
            Time.timeScale = 0f;
    }
    private void powerUpTimer()
    {
        powerUpRemainigTime -= Time.deltaTime;
        if (powerUpRemainigTime <= 0)
            powerUpType = 1;
    }
}
