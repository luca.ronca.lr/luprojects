using UnityEngine;

public class AsteroidsSpawner : MonoBehaviour
{
    [SerializeField] private GameObject[] asteroidsArray = new GameObject[16];
    [SerializeField] private Transform spawner;
    [SerializeField] private float spawnAsteroidInterval;

    private float currentTime;
    private void Start()
    {
        spawnAsteroidInterval = PlayerPrefs.GetFloat("AsteriodSpawnInterval");

        currentTime = spawnAsteroidInterval;
    }
    private void Update()
    {
        if (Player.remainingTime < 3 || Player.gameOver)
            return;

        if (currentTime <= 0)
        {
            currentTime = spawnAsteroidInterval;
            float scale = Random.Range(1, 2);
            Spawning(asteroidsArray, spawner, 30);
        }
        else
            Delay();
    }
    private void Spawning(GameObject[] asteroids, Transform spawnPoint, float scale)
    {
        spawnPoint.position = new Vector3(Random.Range(-50f, 50f), 0f, spawnPoint.position.z);
        int position = Random.Range(0, asteroids.Length - 1);
        GameObject asteroid = asteroids[position];
        asteroid.transform.localScale = new Vector3(scale,scale,scale);
        Instantiate(asteroid, spawnPoint.position, spawnPoint.rotation);
        if (position > 12)
            Player.specialAsteroidNumber ++;
    }
    private void Delay()
        => currentTime -= Time.deltaTime;
}
