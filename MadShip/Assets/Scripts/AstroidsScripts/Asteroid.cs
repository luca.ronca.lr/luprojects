using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Asteroid : MonoBehaviour
{
    [SerializeField] private GameObject buff, debuff;
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("DownCollider"))
            Destroy(this.gameObject); 
        if(collision.gameObject.CompareTag("Ship"))
        {
            ParticleEffects.explosionRedInstance.Play();
            AudioPlayer.explosionShipSound.Play();
        }
        if (collision.gameObject.CompareTag("Bullet"))
        {
            if(this.gameObject.CompareTag("SpecialAsteroid") && SceneManager.GetActiveScene().name.Equals("Level_25"))
            {
                if (Random.Range(0f, 10f) > 3.5f)
                    spawnPowerUp(buff);
                else
                    spawnPowerUp(debuff);
            }
            AudioPlayer.explosionAsteroidSound.Play();
            Destroy(gameObject);
        }
    }
    private void spawnPowerUp(GameObject powerUpPrefab)
    {
        Vector3 position = new Vector3(transform.position.x, -30f, transform.position.z);
        var powerUp = Instantiate(powerUpPrefab, position, powerUpPrefab.transform.rotation);
        powerUp.transform.localScale = powerUpPrefab.transform.localScale;
    }
}
