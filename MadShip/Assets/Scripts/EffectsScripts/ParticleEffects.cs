using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffects : MonoBehaviour
{
    [SerializeField] private ParticleSystem explosionRedEffect, explosionOrangeEffect, engineEffect;
    public static ParticleSystem explosionRedInstance, explosionOrangeInstance, engineInstance;
    void Start()
    {
        explosionRedInstance = explosionRedEffect;
        explosionOrangeInstance = explosionOrangeEffect;
        engineInstance = engineEffect;

        explosionRedInstance.Stop();
        explosionOrangeInstance.Stop();
    }
}
