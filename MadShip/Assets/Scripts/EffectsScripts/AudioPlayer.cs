using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{

    [SerializeField] private AudioSource explosionAsteroid, explosionShip, laser, victory, lose, level;

    public static AudioSource explosionAsteroidSound, explosionShipSound, laserSound, victorySound, loseSound, levelSound;
    private void Start()
    {
        explosionAsteroidSound = explosionAsteroid;
        explosionShipSound = explosionShip; 
        laserSound = laser;
        victorySound = victory;
        loseSound = lose;
        levelSound = level;
    }
}
