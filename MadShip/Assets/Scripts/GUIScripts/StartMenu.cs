using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Unity.VisualScripting;

public class StartMenu : MonoBehaviour
{
    [SerializeField] private GameObject buttons, optionMenu;
    [SerializeField] private TMP_Dropdown difficulty;

    private float cameraSpeed, movementSpeed, asteriodSpawnInterval;
    void Start()
    {
        buttons.SetActive(true);
        optionMenu.SetActive(false);

        cameraSpeed = 1.25f;
        movementSpeed = 100f;
        asteriodSpawnInterval = 1f;
    }
    public void StartButton()
    {
        PlayerPrefs.SetFloat("CameraSpeed", cameraSpeed);
        PlayerPrefs.SetFloat("MovementSpeed", movementSpeed);
        PlayerPrefs.SetFloat("AsteriodSpawnInterval", asteriodSpawnInterval);
        SceneManager.LoadScene("Level_0");
    }

    public void OptionButton()
    {
        buttons.SetActive(false);
        optionMenu.SetActive(true);
    }
    public void ContinueButton()
    {
        buttons.SetActive(true);
        optionMenu.SetActive(false);
    }
    public void ExitButton()
        => Application.Quit();
    public void HandleInputData(int value)
    {
        
        switch(value)
        {
            case 0:
                cameraSpeed = 0.5f;
                movementSpeed = 80f;
                asteriodSpawnInterval = 1.1f;
                break;
            case 1:
                cameraSpeed = 1.25f;
                movementSpeed = 100f;
                asteriodSpawnInterval = 1f;
                break;
            case 2:
                cameraSpeed = 2f;
                movementSpeed = 120f;
                asteriodSpawnInterval = 0.9f;
                break;
        }
    }
}
