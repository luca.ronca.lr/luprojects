using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class UIMenu : MonoBehaviour
{
    [SerializeField] private GameObject gameOverMenu, winMenu, perfect;
    [SerializeField] private TMP_Text loseStats, winStats, level, score;
    [SerializeField] private Image powerUp, progressBar; 
    [SerializeField] private Sprite buff, debuff;

    void Start ()
    {
        gameOverMenu.SetActive(false);
        winMenu.SetActive(false);
        perfect.SetActive(false);
        level.text = SceneManager.GetActiveScene().name;
    }
    void Update()
    {
        progressBar.rectTransform.sizeDelta = new Vector2(progressBar.rectTransform.sizeDelta.x, (60 - Player.remainingTime) * 4);
        score.text = $"BlueAsteroid: {Player.score}";
        if (Player.remainingToFinish <=0 && Player.gameOver)
        {
            gameOverMenu.SetActive(true);
            loseStats.text = $"Special Asteroid Destroided\r\n{Player.score} / {Player.specialAsteroidNumber}";
            AudioPlayer.levelSound.Stop();
        }  
        if (Player.remainingToFinish <= 0 &&  Player.win)
        {
            winMenu.SetActive(true);
            winStats.text = $"Special Asteroid Destroided\r\n{Player.score} / {Player.specialAsteroidNumber}";
            if (Player.score == Player.specialAsteroidNumber)
                perfect.SetActive(true);
            AudioPlayer.levelSound.Stop();
        }
        Color color;
        switch (Player.powerUpType)
        {
            case 0: 
                powerUp.sprite = debuff;
                color = powerUp.color;
                color = new Color(color.r, color.g, color.b, 2.25f);
                powerUp.color = color;
                break;
            case 1:
                powerUp.sprite = buff;
                color = powerUp.color;
                color = new Color(color.r, color.g, color.b, 0.1f);
                powerUp.color = color;
                break; 
            case 2:
                powerUp.sprite = buff;
                color = powerUp.color;
                color = new Color(color.r, color.g, color.b, 2.25f);
                powerUp.color = color;
                break;
        }
    }
    private void FixedUpdate()
    {
        float unscaledDeltaTime = Time.fixedUnscaledDeltaTime;
        if (Player.win)
            AudioPlayer.victorySound.Play();
        if (Player.gameOver)
            AudioPlayer.loseSound.Play();
    }
    public void LoadingSceneButton(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
